import 'package:firebase_analytics/firebase_analytics.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'dart:convert';
import 'package:http/http.dart' as http;

class JWTPage extends StatefulWidget {
  @override
  _JWTPageState createState() => _JWTPageState();
}

class _JWTPageState extends State<JWTPage> {
  final TextEditingController _username = TextEditingController();
  final TextEditingController _password = TextEditingController();
  final TextEditingController _token = TextEditingController();
  final TextEditingController _data = TextEditingController();
  String alertTitle = "";
  var alertBody;
  var serverData;

  makeRequestToGetToken(context, username, password) async{
    http.post('https://mobdev-191-331-dbor.herokuapp.com/auth',
              headers: {'Content-Type':'application/json; charset=utf-8'},
              body: jsonEncode({'username':'$username','password':'$password'})).
    then((response) {
      if(response.statusCode == 200)
        setState(() {
        //print("Response body: ${response.body}");
          _token.text = jsonDecode(response.body)["access_token"];
        });
      else {
        setState(() {
          alertTitle = "Ошибка авторизации";
          alertBody = jsonDecode(response.body);
        });
        showAlertDialog(context);
      }
    }).catchError((error){
      print("Error: $error");
    });
  }

  makeRequestWithToken(context, access_token) async{
    http.get('https://mobdev-191-331-dbor.herokuapp.com/protected',
        headers: {'Content-Type':'application/json; charset=utf-8', 'Authorization':'JWT $access_token'},
    ).
    then((response) {
      if(response.statusCode == 200)
        setState(() {
          print("Response body: ${response.body}");
          //_data.text = response.body.toString();
          serverData = jsonDecode(response.body);
        });
      else {
        setState(() {
          alertTitle = "Ошибка при получении данных";
          alertBody = jsonDecode(response.body);
        });
        showAlertDialog(context);
      }
    }).catchError((error){
      print("Error: $error");
    });
  }

  showAlertDialog(BuildContext context) {
    // set up the button
    Widget okButton = ElevatedButton(
      child: Text("OK"),
      onPressed: () {
        Navigator.of(context).pop();
      },
    );
    // set up the AlertDialog
    AlertDialog alert = AlertDialog(
      title: Text(alertTitle),
      content: SingleChildScrollView(child: ListBody(
      children: <Widget>[
        Text("Описание ошибки: ${alertBody["description"]}"),
        Text("Тип ошибки: ${alertBody["error"]}"),
        Text("Код ошибки: ${alertBody["status_code"]}"),
      ]
    ),),
      actions: [
        okButton,
      ],
    );
    // show the dialog
    showDialog(
      context: context,
      builder: (BuildContext context) {
        return alert;
      },
    );
  }

  showServerInfo(BuildContext context) {
    // set up the button
    Widget okButton = ElevatedButton(
      child: Text("ок"),
      onPressed: () {
        Navigator.of(context).pop();
      },
    );
    // set up the AlertDialog
    AlertDialog alert = AlertDialog(
      title: Text('Успех'),
      content: SingleChildScrollView(child: Padding(
          padding: const EdgeInsets.all(15.0),
          child: Column(
              children: [
                Text("Сообщение: ${serverData["response"]}"),
                SizedBox(
                  height: 15,
                ),
                Image.memory(base64Decode(serverData["img"].replaceAll("\n", ''))),
                Text("Дата: ${serverData["date"]}"),
              ]
          )
      ),
      ),
      actions: [
        okButton,
      ],
    );
    // show the dialog
    showDialog(
      context: context,
      builder: (BuildContext context) {
        return alert;
      },
    );
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text("JWT lab"),
        backgroundColor: Colors.black,
      ),
      body: SingleChildScrollView(
        child: Padding(
          padding: const EdgeInsets.all(15.0),
          child: Column(
            children: [
              TextField(
                //obscureText: true,
                //maxLength: 32,
                controller: _username,
                style: TextStyle(color: Colors.white, fontSize: 18),
                decoration: InputDecoration(
                  enabledBorder: OutlineInputBorder(
                    borderSide: BorderSide(
                        color: Colors.white, width: 2.0),
                  ),
                  focusedBorder: OutlineInputBorder(
                    borderSide: BorderSide(
                        color: Colors.blue, width: 2.0),
                  ),
                  labelText: 'Введите логин',
                  labelStyle: TextStyle(color: Colors.white),
                  filled: true,
                  fillColor: Colors.white12,
                ),
              ),
              SizedBox(
                height: 15,
              ),
              TextField(
                //obscureText: true,
                //maxLength: 32,
                obscureText: true,
                controller: _password,
                style: TextStyle(color: Colors.white, fontSize: 18),
                decoration: InputDecoration(
                  enabledBorder: OutlineInputBorder(
                    borderSide: BorderSide(
                        color: Colors.white, width: 2.0),
                  ),
                  focusedBorder: OutlineInputBorder(
                    borderSide: BorderSide(
                        color: Colors.blue, width: 2.0),
                  ),
                  labelText: 'Введите пароль',
                  labelStyle: TextStyle(color: Colors.white),
                  filled: true,
                  fillColor: Colors.white12,
                ),
              ),
              SizedBox(
                height: 15,
              ),
              ElevatedButton(
                  onPressed: () {
                    FirebaseAnalytics().logEvent(name: 'This_is_get_token_button',parameters:null);
                    if(_username.text.isNotEmpty && _password.text.isNotEmpty)
                      makeRequestToGetToken(context, _username.text, _password.text);
                    else
                      showAlertDialog(context);
                  },
                  child: Row(
                    mainAxisAlignment: MainAxisAlignment.center,
                    mainAxisSize: MainAxisSize.min,
                    children: [
                      Text('Запросить JWT-токен',
                          style: TextStyle(fontWeight: FontWeight.bold, fontSize: 18)),
                    ],
                  ),
                  style: ButtonStyle()
              ),
              SizedBox(
                height: 15,
              ),
              TextField(
                //obscureText: true,
                //maxLength: 32,
                enabled: false,
                keyboardType: TextInputType.multiline,
                minLines: 1,
                maxLines: null,
                controller: _token,
                style: TextStyle(color: Colors.white, fontSize: 18),
                decoration: InputDecoration(
                  enabledBorder: OutlineInputBorder(
                    borderSide: BorderSide(
                        color: Colors.white, width: 2.0),
                  ),
                  focusedBorder: OutlineInputBorder(
                    borderSide: BorderSide(
                        color: Colors.blue, width: 2.0),
                  ),
                  labelText: 'Access token',
                  labelStyle: TextStyle(color: Colors.white),
                  filled: true,
                  fillColor: Colors.white12,
                ),
              ),
              SizedBox(
                height: 15,
              ),
              ElevatedButton(
                  onPressed: () async {
                    if(_token.text.isNotEmpty) {
                      await makeRequestWithToken(context, _token.text);
                      showServerInfo(context);
                    }
                    else
                      showAlertDialog(context);
                  },
                  child: Row(
                    mainAxisAlignment: MainAxisAlignment.center,
                    mainAxisSize: MainAxisSize.min,
                    children: [
                      Container(
                        child: Text('Запрос к API-методу',
                            style: TextStyle(fontWeight: FontWeight.bold, fontSize: 18)),
                      ),
                    ],
                  ),
                  style: ButtonStyle()
              ),

            ],
          ),
        ),
      ),
      backgroundColor: new Color.fromARGB(255, 45, 45, 45),
    );
  }
}