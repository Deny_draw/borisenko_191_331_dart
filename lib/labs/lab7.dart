import 'package:flutter/cupertino.dart';
import 'package:web_socket_channel/web_socket_channel.dart';
import 'package:flutter/material.dart';
import 'package:bubble/bubble.dart';
import 'dart:convert';
import 'package:intl/intl.dart';

class WebSocketPage extends StatefulWidget {

  @override
  _WebSocketPageState createState() => _WebSocketPageState();
}

class Message{
  String text;
  bool type;
  String time;
  String img;

  Message(String _text, bool _type, String _time, String _img){
    text = _text;
    type = _type;
    time = _time;
    img = _img;
  }
}

class _WebSocketPageState extends State<WebSocketPage> {
  final TextEditingController _controller = TextEditingController();

  final _channel = WebSocketChannel.connect(
    Uri.parse('ws://mobdev.std-1223.ist.mospolytech.ru:80/'),
  );
  List<Message> loadedMessages = [];
  final ScrollController _scrollController = ScrollController();

  @override
  Widget build(BuildContext context) {
    return WillPopScope(
      onWillPop: () async {
        dispose();
        return true;
      },
      child: Scaffold(
        appBar: AppBar(
          title: Text("Чат WebSocket"),
          backgroundColor: Colors.black,
        ),
        body: Stack(
          children: <Widget>[
            StreamBuilder(
                stream: _channel.stream, builder: (context, snapshot) {
              if(snapshot.hasData){
                var response = jsonDecode(snapshot.data);
                //print(response["response"]);
                loadedMessages.add(Message(response["response"], false, response["date"], response["img"]));
              }
              return ListView.builder(
                controller: _scrollController,
                itemCount: loadedMessages.length,
                shrinkWrap: true,
                padding: EdgeInsets.only(top: 10, bottom: 10),
                itemBuilder: (context, index) {
                  return Bubble(
                    padding: BubbleEdges.all(10.0),
                    margin: BubbleEdges.only(
                        top: 15.0, bottom: index == loadedMessages.length-1 ? 70.0 : 0.0,
                        left: loadedMessages[index].type ? 70 : 0,
                        right: loadedMessages[index].type ? 0 : 70),
                    nip:
                    loadedMessages[index].type ? BubbleNip.rightBottom : BubbleNip.leftBottom,
                    child: Column(
                      children: [
                        Align(
                            alignment: loadedMessages[index].type ? Alignment.topRight : Alignment.topLeft,
                            child: loadedMessages[index].img.isNotEmpty
                                ? Image.network(loadedMessages[index].img, height: 200, width: 200,)
                                : Text(loadedMessages[index].text, style: TextStyle(fontSize: 16, color: Colors.white))
                        ),
                        Padding(
                          padding: const EdgeInsets.only(top: 10),
                          child: Row(
                            mainAxisAlignment: loadedMessages[index].type ? MainAxisAlignment.start : MainAxisAlignment.end,
                              children: [
                                Text(loadedMessages[index].type ? "исходящее    " : "входящее    ", style: TextStyle(fontSize: 10, color: Colors.white)),
                                Text(loadedMessages[index].time, style: TextStyle(fontSize: 10, color: Colors.white))
                              ]
                          ),
                        ),
                      ]
                    ),
                    alignment: loadedMessages[index].type
                        ? Alignment.centerRight
                        : Alignment.centerLeft,
                    color: loadedMessages[index].type
                        ? Color.fromARGB(255, 55, 173, 95)
                        : Color.fromARGB(255, 29, 172, 214),
                  );
                },
              );
            }),
            Align(
              alignment: Alignment.bottomLeft,
              child: Container(
                padding: EdgeInsets.only(left: 10, bottom: 10, top: 10),
                height: 60,
                width: double.infinity,
                color: Colors.white,
                child: Row(
                  children: <Widget>[
                    SizedBox(
                      width: 5,
                    ),
                    Expanded(
                      child: TextField(
                        controller: _controller,
                        decoration: InputDecoration(
                            hintText: "Отправляемое сообщение",
                            hintStyle: TextStyle(color: Colors.black),
                            border: InputBorder.none),
                      ),
                    ),
                    SizedBox(
                      width: 15,
                    ),
                    ElevatedButton(
                      onPressed: () {
                        if (_controller.text.isNotEmpty) {
                          _sendMessage();
                        }
                      },
                      child: Icon(
                        Icons.send_sharp,
                        color: Colors.white,
                        size: 25,
                      ),
                      style: ButtonStyle(backgroundColor: MaterialStateProperty.all<Color>(
                          Colors.lightBlue[700])),
                    ),
                    SizedBox(
                      width: 15,
                    ),
                  ],
                ),
              ),
            ),
          ],
        ),
        backgroundColor: new Color.fromARGB(255, 45, 45, 45),
      ),
    );
  }

  void _sendMessage() {
    if (_controller.text.isNotEmpty) {
      _channel.sink.add(_controller.text);
      //var time = DateTime.now();
      String time = DateFormat("kk:mm dd.mm.yyyy").format(DateTime.now());
      loadedMessages.add(Message(_controller.text, true, time, ""));
      _controller.clear();
    }
  }

  @override
  void dispose() {
    _channel.sink.close();
    super.dispose();
  }
}