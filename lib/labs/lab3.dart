import 'package:html/parser.dart' show parse;
import 'package:flutter/material.dart';
import 'package:http/http.dart' as http;
import 'package:webview_flutter/webview_flutter.dart';
import 'dart:async';
import 'package:flutter_html/flutter_html.dart';

class WorkWithRequests extends StatefulWidget {
  @override
  _WorkWithRequestsState createState() => new _WorkWithRequestsState();
}

class _WorkWithRequestsState extends State<WorkWithRequests> {
  String data = "";
  var htmlData = "";

  _loadData() async {
    final response = await http.get('https://pub.dev/packages/http');
    if (response.statusCode == 200) {
      var document = parse(response.body);
      print(document
          .getElementsByClassName('likes-count')[0]
          .text);
      print(response.body);
      setState(() {
        htmlData = response.body;
        data = document
            .getElementsByClassName('likes-count')[0]
            .text;
      });
    }
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
        appBar: AppBar(
          title: Text("Requests"), //getAppTitle(_selectedIndex)),
          backgroundColor: Colors.black,
          actions: <Widget>[
            Padding(
              padding: const EdgeInsets.all(10.0),
              child: ElevatedButton(
                onPressed: () {
                  _loadData();
                },
                child: Text("Загрузить данные"),
              ),
            ),
          ],
        ),
        body: PageView(
          children: [
            Container(
                child: SingleChildScrollView(
                    child: Padding(
                      padding: const EdgeInsets.all(10.0),
                      child: Text(htmlData,
                          style:
                              TextStyle(fontSize: 16)),
                    ))),
            Container(
                child: SingleChildScrollView(
                    child: Padding(
              padding: const EdgeInsets.all(10.0),
              child: Html(data: htmlData),
            ))),
          ],
        ),
        bottomNavigationBar: Container(
          child: Row(
            mainAxisAlignment: MainAxisAlignment.center,
            children: [
              Text(data != "" ? "Количество лайков: $data ❤" : "",
                  style: TextStyle(color: Colors.white, fontSize: 18)),
            ],
          ),
          padding: EdgeInsets.all(20),
          color: Colors.black,
        ),
        backgroundColor: Colors.white);
  }
}
