import 'dart:async';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:encrypt/encrypt.dart' as Enc;
import 'package:path_provider/path_provider.dart';
import 'dart:io';

class EncryptionPage extends StatefulWidget {
  @override
  createState() => new EncryptionPageState();
}

class EncryptionPageState extends State<EncryptionPage> {
  TextEditingController keyController = new TextEditingController();
  TextEditingController textController = new TextEditingController();
  String _enc;
  String _dec;
  bool flag = false;
  String alertTitle = '';
  String alertBody = '';
  String textKey;

  _write(String text) async {
    final directory = await getExternalStorageDirectory();
    print(directory.path);
    final file = File('${directory.path}/enc_file.txt');
    await file.writeAsString(text);
    //_read();
  }

  Future<String> _read() async {
    String text;
    try {
      final directory = await getExternalStorageDirectory();
      final file = File('${directory.path}/enc_file.txt');
      text = await file.readAsString();
    } catch (e) {
      print("Couldn't read file");
    }
    return text;
  }

  Future<void> encrypt() async {
    final plainText = textController.text;
    setState(() {
      textKey = keyController.text;
      if(textKey.length < 32){
        while (textKey.length < 32){
          textKey += '0';
        }
      }
      keyController.text = textKey;
    });
    //print(textKey);
    final key = Enc.Key.fromUtf8(textKey);
    final iv = Enc.IV.fromLength(16);
    final encrypter = Enc.Encrypter(Enc.AES(key, padding: null));
    final encrypted = encrypter.encrypt(plainText, iv: iv);
    _enc = encrypted.base64.toString();
    print(encrypted.base64);
    //return _enc;
  }

  Future<void> decrypt() async {
    final plainText = await _read();
    final key = Enc.Key.fromUtf8(textKey);
    final iv = Enc.IV.fromLength(16);
    final encrypter = Enc.Encrypter(Enc.AES(key, padding: null));
    final encrypted = Enc.Encrypted.fromBase64(plainText);
    //print('aaaaaaaaaaaaaaaa');
    /*print(encrypted.base64.toString());*/
    final decrypted = encrypter.decrypt(encrypted, iv: iv);
    _dec = decrypted;
    print(decrypted);
    //return _dec;
  }

  showAlertDialog(BuildContext context) {
    // set up the button
    Widget okButton = ElevatedButton(
      child: Text("OK"),
      onPressed: () {
        Navigator.of(context).pop();
      },
    );
    // set up the AlertDialog
    AlertDialog alert = AlertDialog(
      title: Text(alertTitle),
      content: SingleChildScrollView(child: Text(alertBody)),
      actions: [
        okButton,
      ],
    );
    // show the dialog
    showDialog(
      context: context,
      builder: (BuildContext context) {
        return alert;
      },
    );
  }

  @override
  Widget build(BuildContext context) {
    double width = MediaQuery.of(context).size.width - 30;
    return Scaffold(
        appBar: AppBar(
          title: Text("Data encryption"),
          backgroundColor: Colors.black,
          actions: <Widget>[
            Icon(
              Icons.refresh,
              size: 30.0,
            ),
            Icon(
              Icons.search,
              size: 30.0,
            ),
            Icon(
              Icons.edit,
              size: 30.0,
            ),
            Icon(
              Icons.more_vert,
              size: 30.0,
            ),
          ],
        ),
        body: SingleChildScrollView(
          child: Column(
            //mainAxisAlignment: MainAxisAlignment.center,
            children: [
              Container(
                width: width,
                height: 75,
                padding: EdgeInsets.all(15),
                child: Center(
                    child: Text(
                      "Введите текст для шифрования/расшифрования",
                      style: TextStyle(fontSize: 20, color: Colors.white),
                      textAlign: TextAlign.center,
                      softWrap: true,
                    )),
              ),
              Padding(
                padding: EdgeInsets.all(15),
                child: Row(
                  children: [
                    Column(children: [
                      SizedBox(
                          width: width,
                          height: 60,
                          child: TextField(
                            //obscureText: true,
                            controller: textController,
                            style: TextStyle(color: Colors.white, fontSize: 18),
                            decoration: InputDecoration(
                              enabledBorder: OutlineInputBorder(
                                borderSide: BorderSide(
                                    color: Colors.white, width: 2.0),
                              ),
                              focusedBorder: OutlineInputBorder(
                                borderSide: BorderSide(
                                    color: Colors.blue, width: 2.0),
                              ),
                              labelText: 'Введите текст',
                              labelStyle: TextStyle(color: Colors.white),
                              filled: true,
                              fillColor: Colors.white12,
                            ),
                          ))
                    ]),
                  ],
                ),
              ),
              Container(
                width: width,
                height: 75,
                padding: EdgeInsets.all(15),
                child: Center(
                    child: Text(
                  "Введите ключ шифрования длиной 32 символа",
                  style: TextStyle(fontSize: 20, color: Colors.white),
                  textAlign: TextAlign.center,
                  softWrap: true,
                )),
              ),
              Padding(
                padding: EdgeInsets.all(15),
                child: Row(
                  children: [
                    Column(children: [
                      SizedBox(
                          width: width,
                          height: 100,
                          child: TextField(
                            //obscureText: true,
                            maxLength: 32,
                            controller: keyController,
                            style: TextStyle(color: Colors.white, fontSize: 18),
                            decoration: InputDecoration(
                              enabledBorder: OutlineInputBorder(
                                borderSide: BorderSide(
                                    color: Colors.white, width: 2.0),
                              ),
                              focusedBorder: OutlineInputBorder(
                                borderSide: BorderSide(
                                    color: Colors.blue, width: 2.0),
                              ),
                              labelText: 'Введите ключ',
                              labelStyle: TextStyle(color: Colors.white),
                              filled: true,
                              fillColor: Colors.white12,
                            ),
                            onChanged: (String value) {
                              setState(() {
                                flag = false;
                                });
                              },
                          ))
                    ]),
                  ],
                ),
              ),
              Padding(
                  padding: EdgeInsets.symmetric(vertical: 15, horizontal: 15),
                  child: Row(
                      mainAxisAlignment: MainAxisAlignment.spaceBetween,
                      children: [
                        ElevatedButton(
                            onPressed: () {
                              if(textController.text != '') {
                                encrypt();
                                _write(_enc);
                                setState(() {
                                  flag = true;
                                });
                                alertTitle = "Зашифрованный текст";
                                alertBody = _enc;
                              }
                              else if(textController.text == ''){
                                alertTitle = "Ошибка";
                                alertBody = "В поле сообщения пусто, введите сообщение";
                              }
                              else{
                                alertTitle = "Ошибка";
                                alertBody = "Длина ключа меньше 32 или ключ не задан, введите корректный ключ";
                              }
                              showAlertDialog(context);
                            },
                            child: Row(
                              mainAxisAlignment: MainAxisAlignment.center,
                              mainAxisSize: MainAxisSize.min,
                              children: [
                                Text('Зашифровать',
                                    style: TextStyle(
                                        fontWeight: FontWeight.bold,
                                        fontSize: 18)),
                              ],
                            ),
                            style: ButtonStyle(
                                backgroundColor: MaterialStateProperty.all<Color>(
                                    Colors.lightBlue[700]))),
                        ElevatedButton(
                            onPressed: () {
                              //_read();
                              if(flag && textController.text != '') {
                                decrypt();
                                alertTitle = "Расшифрованный текст";
                                alertBody = _dec;
                              }
                              else if(textController.text == ''){
                                alertTitle = "Ошибка";
                                alertBody = "В поле сообщения пусто, введите сообщение";
                              }
                              else{
                                alertTitle = "Ошибка";
                                alertBody = "Для начала зашифруйте текст";
                              }
                              showAlertDialog(context);
                            },
                            child: Row(
                              mainAxisAlignment: MainAxisAlignment.center,
                              mainAxisSize: MainAxisSize.min,
                              children: [
                                Text('Расшифровать',
                                    style: TextStyle(
                                        fontWeight: FontWeight.bold,
                                        fontSize: 18)),
                              ],
                            ),
                            style: ButtonStyle(
                                backgroundColor: MaterialStateProperty.all<Color>(
                                    Colors.lightBlue[700]))),
                      ])),
              //Text(_enc, style: TextStyle(color: Colors.white, fontSize: 18)),
            ],
          ),
        ),
        backgroundColor: new Color.fromARGB(255, 45, 45, 45),
    );
  }
}
