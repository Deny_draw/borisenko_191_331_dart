import 'package:flutter/material.dart';
import 'dart:math';

class GUIElements extends StatefulWidget {
  @override
  _GUIElementsState createState() => new _GUIElementsState();
}

enum SingingCharacter { male, female }

class _GUIElementsState extends State<GUIElements> {
  RangeValues _currentRangeValues = const RangeValues(40, 80);
  final TextEditingController _text = TextEditingController();
  SingingCharacter _character = SingingCharacter.male;
  double _value = 0.0;
  void _setValue(double value) => setState(() => _value = value);
  static const double minValue = 0;
  static const double maxValue = 10;
  static const double minAngle = -160;
  static const double maxAngle = 160;
  static const double sweepAngle = maxAngle - minAngle;
  static const double distanceToAngle = 0.007 * (maxValue - minValue);

  bool isChecked = false;

  Color getColor(Set<MaterialState> states) {
    const Set<MaterialState> interactiveStates = <MaterialState>{
      MaterialState.pressed,
      MaterialState.hovered,
      MaterialState.focused,
    };
    if (states.any(interactiveStates.contains)) {
      return Colors.blue;
    }
    return Colors.blue;
  }

  @override
  Widget build(BuildContext context) {
    double _normalisedValue = (_value - minValue) / (maxValue - minValue);
    double _angle = (minAngle + _normalisedValue * sweepAngle) / 360 * 2 * pi;

    return Scaffold(
      appBar: AppBar(
        title: Text("GUI Elements"), //getAppTitle(_selectedIndex)),
        backgroundColor: Colors.black,
        actions: <Widget>[
          Icon(
            Icons.refresh,
            size: 30.0,
          ),
          Icon(
            Icons.search,
            size: 30.0,
          ),
          Icon(
            Icons.edit,
            size: 30.0,
          ),
          Icon(
            Icons.more_vert,
            size: 30.0,
          ),
        ],
      ),
      body: SingleChildScrollView(
        child: Padding(
          padding: const EdgeInsets.all(20.0),
          child: Column(
            children: [
              GestureDetector(
                onVerticalDragUpdate: (DragUpdateDetails details) {
                  double changeInY =
                      -details.delta.dy; // Note the negative sign
                  double changeInValue = distanceToAngle * changeInY;
                  double newValue = _value + changeInValue;
                  double clippedValue = min(max(newValue, 0), 1);

                  _setValue(clippedValue);
                },
                child: Transform.rotate(
                  angle: _angle,
                  child: ClipOval(
                      child: Container(
                          color: Colors.blue,
                          child: Icon(
                            Icons.arrow_circle_up_outlined,
                            color: Colors.white,
                            size: 100,
                          ))),
                ),
              ),
              Padding(
                padding: const EdgeInsets.only(bottom: 30),
                child: Slider(
                  value: _value,
                  onChanged: _setValue,
                  min: minValue,
                  max: maxValue,
                  divisions: 20,
                  label: _value.round().toString(),
                ),
              ),
              Padding(
                padding: const EdgeInsets.only(bottom: 30),
                child: TextField(
                  //obscureText: true,
                  //maxLength: 32,
                  keyboardType: TextInputType.multiline,
                  minLines: 1,
                  maxLines: null,
                  controller: _text,
                  style: TextStyle(color: Colors.white, fontSize: 18),
                  decoration: InputDecoration(
                    enabledBorder: OutlineInputBorder(
                      borderSide: BorderSide(color: Colors.white, width: 2.0),
                    ),
                    focusedBorder: OutlineInputBorder(
                      borderSide: BorderSide(color: Colors.blue, width: 2.0),
                    ),
                    labelText: 'Введите ФИО',
                    labelStyle: TextStyle(color: Colors.white),
                    filled: true,
                    fillColor: Colors.white12,
                  ),
                ),
              ),
              RangeSlider(
                values: _currentRangeValues,
                min: 0,
                max: 100,
                divisions: 30,
                labels: RangeLabels(
                  _currentRangeValues.start.round().toString(),
                  _currentRangeValues.end.round().toString(),
                ),
                onChanged: (RangeValues values) {
                  setState(() {
                    _currentRangeValues = values;
                  });
                },
              ),
              Column(
                children: <Widget>[
                  ListTile(
                    title: const Text('Мужской',
                        style: TextStyle(color: Colors.white, fontSize: 18)),
                    leading: Radio<SingingCharacter>(
                      fillColor: MaterialStateProperty.all<Color>(
                          Colors.lightBlue[700]),
                      value: SingingCharacter.male,
                      groupValue: _character,
                      onChanged: (SingingCharacter value) {
                        setState(() {
                          _character = value;
                        });
                      },
                    ),
                  ),
                  ListTile(
                    title: const Text('Женский',
                        style: TextStyle(color: Colors.white, fontSize: 18)),
                    leading: Radio<SingingCharacter>(
                      fillColor: MaterialStateProperty.all<Color>(
                          Colors.lightBlue[700]),
                      value: SingingCharacter.female,
                      groupValue: _character,
                      onChanged: (SingingCharacter value) {
                        setState(() {
                          _character = value;
                        });
                      },
                    ),
                  ),
                ],
              ),
              Padding(
                padding: const EdgeInsets.only(top: 30),
                child: Row(
                  children: [
                    Text("Я прочитал и согласен со всем",
                        style: TextStyle(color: Colors.white, fontSize: 18)),
                    Checkbox(
                      checkColor: Colors.white,
                      fillColor: MaterialStateProperty.resolveWith(getColor),
                      value: isChecked,
                      onChanged: (bool value) {
                        setState(() {
                          isChecked = value;
                        });
                      },
                    )
                  ],
                ),
              ),
            ],
          ),
        ),
      ),
      backgroundColor: new Color.fromARGB(255, 45, 45, 45),
    );
  }
}
