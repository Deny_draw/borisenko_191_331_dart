import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';

class FriendsList extends StatefulWidget {
  final String accessToken;
  final String userId;
  final String userImageUrl;
  final List<dynamic> friendsList;
  final numOfFriends;
  final String userFirstname;
  final String userLastname;

  FriendsList({Key key, this.accessToken, this.userId, this.userImageUrl, this.numOfFriends, this.friendsList, this.userFirstname, this.userLastname}) : super(key: key);

  @override
  createState() => new _FriendsListState();
}

class _FriendsListState extends State<FriendsList> {
  var currentFriend = 0;

  showMyDialog(BuildContext context) {
    // set up the AlertDialog
    AlertDialog alert = AlertDialog(
      title: Text('Подробная информация о пользователе ${widget.friendsList[currentFriend]["first_name"]} ${widget.friendsList[currentFriend]["last_name"]}:'),
      content: SingleChildScrollView(
        child: ListBody(
          children: <Widget>[
            Row(
              mainAxisAlignment: MainAxisAlignment.start,
              children: [
                Image.network("${widget.friendsList[currentFriend]["photo_50"]}"),
                Padding(
                  padding: EdgeInsets.symmetric(horizontal: 15),
                  child: Text('''${widget.friendsList[currentFriend]["first_name"]}\n${widget.friendsList[currentFriend]["last_name"]}'''),
                ),
                Icon(widget.friendsList[currentFriend]["online_mobile"] == 1 ? Icons.phone_android : Icons.circle,
                  color: widget.friendsList[currentFriend]["online"] == 1 ? Colors.green : Colors.grey,
                  size: widget.friendsList[currentFriend]["online_mobile"] == 1 ? 15.0 : 10.0,)
              ],
            ),
            Padding(
              padding: EdgeInsets.symmetric(vertical: 10),
              child: ListBody(
                  children: <Widget>[
                    widget.friendsList[currentFriend]["city"] != null ? widget.friendsList[currentFriend]["city"]["title"] != "" ?
                    Text('Город: ${widget.friendsList[currentFriend]["city"]["title"]}')
                        : Text('Город: не задан') : Text('Город: не задан'),
                    widget.friendsList[currentFriend]["country"] != null ? widget.friendsList[currentFriend]["country"]["title"] != "" ?
                    Text('Страна: ${widget.friendsList[currentFriend]["country"]["title"]}')
                        : Text('Страна: не задана') :  Text('Страна: не задана'),
                    widget.friendsList[currentFriend]["sex"] != null ?
                    widget.friendsList[currentFriend]["sex"] != 2? Text('Пол: женский')
                        : Text('Пол: мужской') : Text('Пол: не задан'),
                  ]
              ),
            )
          ],
        ),
      ),
    );
    // show the dialog
    showDialog(
      context: context,
      builder: (BuildContext context) {
        return alert;
      },
    );
  }

  @override
  Widget build(BuildContext context){
    return Scaffold(
      appBar: AppBar(
        title: Text("${widget.userFirstname} ${widget.userLastname}, друзья: ${widget.numOfFriends}", style: TextStyle(fontSize: 18)),
        backgroundColor: Colors.black,
        leading: Image.network(
            widget.userImageUrl),
      ),
      body: ListView.separated(
        physics: BouncingScrollPhysics(),
        padding: EdgeInsets.all(10),
        itemCount: widget.numOfFriends,
        itemBuilder: (context, index) {
          return TextButton(
            onPressed: (){
              setState(() {
                currentFriend = index;
              });
              showMyDialog(context);
              },
            child: Container(
              child: Row(
                mainAxisAlignment: MainAxisAlignment.start,
                children: [
                  Image.network("${widget.friendsList[index]["photo_50"]}"),
                  Padding(
                    padding: EdgeInsets.symmetric(horizontal: 10),
                        child: Text("${widget.friendsList[index]["first_name"]} ${widget.friendsList[index]["last_name"]}", style: TextStyle(color: Colors.white, fontSize: 14)),
                  ),
                  Icon(widget.friendsList[index]["online_mobile"] == 1 ? Icons.phone_android : Icons.circle,
                    color: widget.friendsList[index]["online"] == 1 ? Colors.green : Colors.grey,
                    size: widget.friendsList[index]["online_mobile"] == 1 ? 15.0 : 10.0,)
                ],
              ),
          ),
            /*child: Text("${widget.friendsList[index]}", style: TextStyle(color: Colors.white)),*/
          );
        },
        separatorBuilder: (context, index) => Divider(
          color: Colors.black,
          thickness: 2,
        ),
      ),
      /*Container(
        child: ElevatedButton(
          onPressed: getFriendsList,
          child: Text("жмякни"),
        )
      ),*/
        bottomNavigationBar: Container(
          child: Row(
            mainAxisAlignment: MainAxisAlignment.spaceEvenly,
            children: [
              Text("Запросы по REST API (lab5)  ", style: TextStyle(color: Colors.white)),
              ElevatedButton(
                onPressed: () {
                  Navigator.pop(context);
                },
                child: Row(
                  mainAxisAlignment: MainAxisAlignment.center,
                  mainAxisSize: MainAxisSize.min,
                  children: [
                    Text('Вернуться',
                        style: TextStyle(fontWeight: FontWeight.bold, fontSize: 18)),
                    Icon(Icons.arrow_forward, size: 20.0,),
                  ],
                ),
                style: ButtonStyle(
                    backgroundColor:
                    MaterialStateProperty.all<Color>(Colors.lightBlue[700])),
              ),
            ],
          ),
          padding: EdgeInsets.all(12),
          color: Colors.black,
        ),
        /*persistentFooterButtons: [
          ElevatedButton(
            onPressed: () {
              Navigator.pop(context);
            },
            child: Row(
              mainAxisAlignment: MainAxisAlignment.center,
              mainAxisSize: MainAxisSize.min,
              children: [
              Text('Вернуться ',
              style: TextStyle(fontWeight: FontWeight.bold, fontSize: 18)),
              Icon(Icons.arrow_forward, size: 30.0,),
              ],
            ),
            style: ButtonStyle(
            backgroundColor:
            MaterialStateProperty.all<Color>(Colors.lightBlue[700])),
          ),
      ],*/
      backgroundColor: new Color.fromARGB(255, 45, 45, 45),
    );
  }
}

