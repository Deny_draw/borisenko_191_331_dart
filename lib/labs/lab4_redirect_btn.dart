import 'package:firebase_analytics/firebase_analytics.dart';
import 'package:flutter/material.dart';
import 'lab4.dart';

class AuthPage extends StatefulWidget {
  @override
  createState() => new AuthPageState();
}

class AuthPageState extends State<AuthPage> {
  @override
  Widget build(BuildContext context) {
    final String vkLogoPath = 'images/vk_logo.png';
    AssetImage vkLogo = AssetImage(vkLogoPath);

    return Center(
        child: Container(
          child: ElevatedButton(
              onPressed: () {
                FirebaseAnalytics().logEvent(name: 'This_is_VK_authorisation_button',parameters:null);
                FirebaseAnalytics().setCurrentScreen(screenName: "lab4_second_screen");
                Navigator.of(context).push(MaterialPageRoute(
                    builder: (BuildContext context) => LoginScreen()));
              },
              child: Row(
                mainAxisAlignment: MainAxisAlignment.center,
                mainAxisSize: MainAxisSize.min,
                children: [
                  Text('Авторизация через ',
                      style: TextStyle(fontWeight: FontWeight.bold, fontSize: 18)),
                  Image(image: vkLogo, fit: BoxFit.cover, height: 40.0)
                ],
              ),
              style: ButtonStyle(
                  backgroundColor:
                  MaterialStateProperty.all<Color>(Colors.lightBlue[700]))),
        ));
  }
}