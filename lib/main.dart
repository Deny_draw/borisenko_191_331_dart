import 'package:firebase_analytics/firebase_analytics.dart';
import 'package:flutter/material.dart';
import 'labs/lab1.dart';
import 'labs/lab2.dart';
import 'labs/lab3.dart';
import 'labs/lab4_redirect_btn.dart';
import 'labs/lab6.dart';
import 'labs/lab7.dart';
import 'labs/lab8.dart';

class MyApp extends StatelessWidget {
  const MyApp({Key key}) : super(key: key);

  static const String _title = 'Мобильные приложения';

  @override
  Widget build(BuildContext context) {
    FirebaseAnalytics().logAppOpen();
    return MaterialApp(
        debugShowCheckedModeBanner: false,
        title: _title,
        initialRoute: '/',
        //home: MyStatefulWidget(),
        routes: {
          '/': (BuildContext context) => MyStatefulWidget(),
          '/lab1': (BuildContext context) => GUIElements(),
          '/lab2': (BuildContext context) => PhotoAndVideo(),
          '/lab3': (BuildContext context) => WorkWithRequests(),
          '/lab6': (BuildContext context) => EncryptionPage(),
          '/lab7': (BuildContext context) => WebSocketPage(),
          '/lab8': (BuildContext context) => JWTPage(),
        });
  }
}

/// This is the stateful widget that the main application instantiates.
class MyStatefulWidget extends StatefulWidget {
  const MyStatefulWidget({Key key}) : super(key: key);

  @override
  _MyStatefulWidgetState createState() => _MyStatefulWidgetState();
}

/// This is the private State class that goes with MyStatefulWidget.
class _MyStatefulWidgetState extends State<MyStatefulWidget>
    with WidgetsBindingObserver {
  int _selectedIndex = 0;
  static const TextStyle optionStyle =
      TextStyle(fontSize: 30, fontWeight: FontWeight.bold);

  @override
  initState() {
    super.initState();
    WidgetsBinding.instance.addObserver(this);
  }

  @override
  void dispose() {
    WidgetsBinding.instance.removeObserver(this);
    super.dispose();
  }

  @override
  Future<void> didChangeAppLifecycleState(AppLifecycleState state) async {
    switch (state) {
      case AppLifecycleState.resumed:
        {
          await FirebaseAnalytics()
              .logEvent(name: 'app_resumed', parameters: null);
          print("app_resumed");
          break;
        }
      case AppLifecycleState.inactive:
        {
          await FirebaseAnalytics()
              .logEvent(name: 'app_inactive', parameters: null);
          print("app_inactive");
          break;
        }
      case AppLifecycleState.paused:
        {
          await FirebaseAnalytics()
              .logEvent(name: 'app_paused', parameters: null);
          print("app_paused");
          break;
        }
      case AppLifecycleState.detached:
        {
          print("app_closed");
          FirebaseAnalytics()
              .logEvent(name: 'app_closed', parameters: null)
              .then((value) => print("maybe the log will meet firebase T-T"));
          break;
        }
    }
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text("OAuth2"), //getAppTitle(_selectedIndex)),
        backgroundColor: Colors.black,
        actions: <Widget>[
          Icon(
            Icons.refresh,
            size: 30.0,
          ),
          Icon(
            Icons.search,
            size: 30.0,
          ),
          Icon(
            Icons.edit,
            size: 30.0,
          ),
          Icon(
            Icons.more_vert,
            size: 30.0,
          ),
        ],
      ),
      body: AuthPage(),
      backgroundColor: new Color.fromARGB(255, 45, 45, 45),
      drawer: Drawer(
        child: Container(
          color: new Color.fromARGB(255, 65, 65, 65),
          child: ListView(
            padding: EdgeInsets.zero,
            children: <Widget>[
              DrawerHeader(
                child: Center(
                  child: Text('Меню',
                      style: TextStyle(color: Colors.white, fontSize: 20)),
                ),
                decoration: BoxDecoration(
                  color: new Color.fromARGB(255, 45, 45, 45),
                ),
              ),
              ListTile(
                title: Text('lab1',
                    style: TextStyle(color: Colors.white, fontSize: 20)),
                onTap: () {
                  FirebaseAnalytics()
                      .setCurrentScreen(screenName: "lab1_screen");
                  Navigator.pop(context);
                  Navigator.of(context).pushNamed('/lab1');
                },
              ),
              ListTile(
                title: Text('lab2',
                    style: TextStyle(color: Colors.white, fontSize: 20)),
                onTap: () {
                  FirebaseAnalytics()
                      .setCurrentScreen(screenName: "lab2_screen");
                  Navigator.pop(context);
                  Navigator.of(context).pushNamed('/lab2');
                },
              ),
              ListTile(
                title: Text('lab3',
                    style: TextStyle(color: Colors.white, fontSize: 20)),
                onTap: () {
                  FirebaseAnalytics()
                      .setCurrentScreen(screenName: "lab3_screen");
                  Navigator.pop(context);
                  Navigator.of(context).pushNamed('/lab3');
                },
              ),
              ListTile(
                title: Text('lab4 и lab5',
                    style: TextStyle(color: Colors.white, fontSize: 20)),
                onTap: () {
                  FirebaseAnalytics()
                      .setCurrentScreen(screenName: "lab4_first_screen");
                  Navigator.pop(context);
                  Navigator.of(context).pushNamed('/');
                },
              ),
              ListTile(
                title: Text('lab6',
                    style: TextStyle(color: Colors.white, fontSize: 20)),
                onTap: () {
                  FirebaseAnalytics()
                      .setCurrentScreen(screenName: "lab6_screen");
                  Navigator.pop(context);
                  Navigator.of(context).pushNamed('/lab6');
                },
              ),
              ListTile(
                title: Text('lab7',
                    style: TextStyle(color: Colors.white, fontSize: 20)),
                onTap: () {
                  FirebaseAnalytics()
                      .setCurrentScreen(screenName: "lab7_screen");
                  Navigator.pop(context);
                  Navigator.of(context).pushNamed('/lab7');
                },
              ),
              ListTile(
                title: Text('lab8',
                    style: TextStyle(color: Colors.white, fontSize: 20)),
                onTap: () {
                  FirebaseAnalytics()
                      .setCurrentScreen(screenName: "lab8_screen");
                  Navigator.pop(context);
                  Navigator.of(context).pushNamed('/lab8');
                },
              ),
            ],
          ),
        ),
      ),
    );
  }
}

void main() => runApp(const MyApp());
